#!/usr/bin/python
# stopwatch.py for linux -- terminal-based stopwatch
# inspired by @eevee ; generous portions stolen from stackoverflow
# otherwise assembled by perpetual_disbelief
# TODO: moar abstraction, GUI version.  (though someone else probably made it already)

# Known Issue: If you have this open in a virtual terminal window and alt-tab away, the
# display may freeze.  To unfreeze/stop, double-click inside the terminal window.
# (Tested with Sakura terminal; your mileage may vary.)

# Known Issue #2:  "Fancy" keys (numpad, Function keys, etc) which cause more than 1 byte
# to be sent via STDIN will not work for starting the stopwatch; it will start and immediately stop.
# Stick to something safe like spacebar, enter, escape, or a letter.

from datetime import datetime
import sys,os,threading,tty,termios

class _GetchUnix:
    def __call__(self):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class KeyEventThread(threading.Thread):
    def run(self):
		getch = _GetchUnix()
		unused = getch()


def clear():
    """Clear screen, return cursor to top left"""
    sys.stdout.write('\033[2J')
    sys.stdout.write('\033[H')
    sys.stdout.flush()

os.system('setterm -cursor off')
clear()

print "Press any key to start stopwatch."
firstthread = KeyEventThread()
firstthread.start()
waiting=firstthread.is_alive()
while waiting:
	waiting = firstthread.is_alive()

kethread = KeyEventThread()
start = datetime.now()
kethread.start()
waiting=True
while waiting:
	sys.stdout.write('\033[H')
	sys.stdout.flush()
	print "Elapsed time:", datetime.now() - start, "(Press any key to stop)"
	waiting = kethread.is_alive()
	
os.system('setterm -cursor on')
clear()
print "Elapsed time:", datetime.now() - start
